Проверка скорости загрузки сайта
================================
Проверьте, насколько быстро загружается Ваш сайт и что нужно для устранения малой скорости загрузки.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist maksimmartishkin/yii2-pagespeed-insight-url "*"
```

or add

```
"maksimmartishkin/yii2-pagespeed-insight-url": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \maksimmartishkin\pagespeed\AutoloadExample::widget(); ?>```