<?php

namespace maksimmartishkin\pagespeed;

/**
 * pagespeedInsightUrl module definition class
 */
class Pagespeed extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'maksimmartishkin\pagespeed\controllers';

    public $apiKey;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

		/**
		 * Подключение шаблона
		 * @var string
		 */
		$this->layout = 'main.php';

        // custom initialization code goes here
    }

	/**
	 * @param $post
	 *
	 * @return bool|string
	 */
	public function curl($post) {
		$postdata="";
		foreach ($post as $key=>$value) {
			$postdata.=$key."=".rawurlencode($value)."&";
		}
		$ch = \curl_init();
		//curl_setopt ($ch, CURLOPT_URL, "https://www.googleapis.com/pagespeedonline/v4/runPagespeed");
		curl_setopt ($ch, CURLOPT_URL, "https://www.googleapis.com/pagespeedonline/v4/runPagespeed?$postdata");
		curl_setopt ($ch, CURLOPT_HEADER, 0);
//		curl_setopt ($ch, CURLOPT_POST, 1);
//		curl_setopt ($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 2);
//		var_dump($ch);die();
		$exec = curl_exec($ch);
		curl_close($ch);/**/

		return $exec;
	}

	/**
	 * Собираем массив
	 * @param $array
	 *
	 * @return mixed
	 */
	public function PaseSpeedInsightArray($array) {
		foreach($array as $key_psi => $val_psi) {
			if($val_psi['ruleImpact'] == 0) {
				continue;
			}
			$ruleImpact = $val_psi['ruleImpact'];
			$summary_format = $val_psi['summary']['format'];
			foreach($val_psi['summary'] as $key => $value) {
				if(is_array($value)) {
					foreach($value as $val_args) {
						$summary_format = str_replace('{{'. $val_args['key'] .'}}', $val_args['value'], $summary_format);
					}
				}
			}
			$header_ar = [];
			$header = [];
			$urls = [];
			$urls_ar = [];
			$result_format = false;
			foreach($val_psi['urlBlocks'] as $key_url => $urlBlock) {
				$header[$key_url] = $urlBlock['header'];

				foreach($header as $value_url) {
					$header_format = $value_url['format'];
					if(is_array($value_url)) {
						foreach($value_url as $val_args) {

							if(is_array($val_args)) {
								foreach($val_args as $args) {
									$header_format = str_replace('{{'. $args['key'] .'}}', $args['value'], $header_format);
									if($args['key'] == 'LINK') {
										$header_format = str_replace(['{{BEGIN_LINK}}', '{{END_LINK}}'], '', $header_format);
									}
								}

							}
						}
					}
					$header_ar[$key_url] = $header_format;
				}
				if(array_key_exists('urls', $urlBlock)) {
					$urls[$key_url] = $urlBlock['urls'];
					//$urls_ar = [];
					foreach($urls as $value_url) {
						$urls_ar[$key_url] = '';
						//echo '<pre>';echo 'key_url ';print_r($key_url);echo '</pre>';
						for($i = 0; $i < sizeOf($value_url); $i++) {
							foreach($value_url[$i]['result'] as $res) {
								//$urls_ar[$key_url][] = '';
								//echo '<pre>';print_r($res);echo '</pre>';
								//$result_format = false;
								if(!is_array($res)) {
									$result_format = $res;
								}
								if(is_array($res)) {
									foreach($res as $args) {
										//echo '<pre>';print_r(str_replace('{{'. $args['key'] .'}}', $args['value'], $result_format));echo '</pre>';
										$result_format = rawurldecode(str_replace('{{'. $args['key'] .'}}', $args['value'], $result_format));
									}
									//echo '<pre>';echo 'i ';print_r($i);echo '</pre>';
									//$urls_ar[$key_url][] = str_replace('{{'. $args['key'] .'}}', $args['value'], $result_format);
								}
//								var_dump($result_format);die();
							}
							$urls_ar[$key_url] = $result_format;

						}
					}
				}
			}




			$result["$ruleImpact.$key_psi"]['ruleImpact'] = $ruleImpact;
			$result["$ruleImpact.$key_psi"]['localizedRuleName'] = $val_psi['localizedRuleName'];
			$result["$ruleImpact.$key_psi"]['summary'] = $summary_format;
			//$mobile["$ruleImpact.$key_psi"]['header'] = $header;
			$result["$ruleImpact.$key_psi"]['header_ar'] = $header_ar;
			$result["$ruleImpact.$key_psi"]['urls'] = $urls_ar;
			//$mobile["$ruleImpact.$key_psi"]['urls_1'] = $urls;
			//$mobile["$ruleImpact.$key_psi"]['localizedRuleName'] = $val_psi['localizedRuleName'];
			//echo '<pre>';print_r($key_psi);echo '</pre>';
			//echo '<pre>';print_r($val_psi);echo '</pre>';
		}

		arsort($result);
		//array_multisort($result, SORT_DESC, SORT_REGULAR);
		return $result;

	}

	public function PageSpeedInsight()
	{

	}

}
