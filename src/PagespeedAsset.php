<?php
/**
 * Copyright (c) 2019.
 *
 * author: maksim martishkin
 */

namespace maksimmartishkin\pagespeed;

use yii\web\AssetBundle;

class PagespeedAsset extends AssetBundle
{
	public $sourcePath = '@maksimmartishkin/pagespeed/assets';
	public $css = [
		'pagespeed.css',
	];
	public $js = [
		'pagespeed.js'
	];
	public $depends = [
		'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',
	];
}