/*
 * Copyright (c) 2019.
 *
 * author: maksim martishkin
 */
var check_url = false;

function isValidUrl(url)
{
    var objRE = /(^https?:\/\/)?[a-z0-9~_\-\.]+\.[a-z]{2,9}(\/|:|\?[!-~]*)?$/i;
    return objRE.test(url);
}

PageSpeed = {
    isValidURL: function (url) {
        if (isValidUrl(url)) {
            $('.input_check_url').removeClass('has-error').addClass('has-success');
            check_url = url;
            $('.js_result_check_url').html('');
        } else {
            $('.input_check_url').removeClass('has-success').addClass('has-error');
        }
    },
    check: function () {
        if(!check_url) {
            var url_check = $('.js-check_url').val();
            if (!isValidUrl(url_check)) {
                $('.js_result_check_url').html('<div class="alert alert-danger alert-dismissable text-center"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Внимание!</strong> Данный урл не валидный</div>');
                return;
            }
            check_url = url_check;
        }

        $.ajax({
            type: 'POST',
            url: '/pagespeed/default/requestpsi',
            data: 'check_url=' + check_url,
            timeout: 50000,
            dataType: 'html',
            beforeSend: function() {
                $('.spinner_check_url').show('fade');
                $('#check_url_btn').attr('disabled', 'disabled');
                $('.js_result_check_url').empty();
            },
            success: function(result){
                $('.js_result_check_url').html($(result).find('.site-about').html());
                var result_psi_mob = $(result).find('.js_scope_json_mob').html();
                var result_psi_desk = $(result).find('.js_scope_json_desk').html();
                var img_result_psi_mob = StatsImgResult(JSON.parse(result_psi_mob));
                var img_result_psi_desk = StatsImgResult(JSON.parse(result_psi_desk));
                $('#img_result_psi_mob').html(img_result_psi_mob);
                $('#img_result_psi_desk').html(img_result_psi_desk);
                $('#img_result_psi_mob').children().addClass('img_psi_mob');
                $('#img_result_psi_desk').children().addClass('img_psi_desk');
            },
            error: function(xhr) { // if error occured
                alert("Error occured.please try again");
                $('.spinner_check_url').hide('fade');
                $('#check_url_btn').attr('disabled', false);
            },
            complete: function() {
                $('.spinner_check_url').hide('fade');
                $('#check_url_btn').attr('disabled', false);
            },
        });
    },

}

var RESOURCE_TYPE_INFO = [
    {label: 'JavaScript', field: 'javascriptResponseBytes', color: 'e2192c'},
    {label: 'Images', field: 'imageResponseBytes', color: 'f3ed4a'},
    {label: 'CSS', field: 'cssResponseBytes', color: 'ff7008'},
    {label: 'HTML', field: 'htmlResponseBytes', color: '43c121'},
    {label: 'Flash', field: 'flashResponseBytes', color: 'f8ce44'},
    {label: 'Text', field: 'textResponseBytes', color: 'ad6bc5'},
    {label: 'Other', field: 'otherResponseBytes', color: '1051e8'},
];

function StatsImgResult(stats) {
    var labels = [];
    var data = [];
    var colors = [];
    var totalBytes = 0;
    var largestSingleCategory = 0;
    for (var i = 0, len = RESOURCE_TYPE_INFO.length; i < len; ++i) {
        var label = RESOURCE_TYPE_INFO[i].label;
        var field = RESOURCE_TYPE_INFO[i].field;
        var color = RESOURCE_TYPE_INFO[i].color;
        if (field in stats) {
            var val = Number(stats[field]);
            totalBytes += val;
            if (val > largestSingleCategory) largestSingleCategory = val;
            labels.push(label);
            data.push(val);
            colors.push(color);
        }
    }
    // Construct the query to send to the Google Chart Tools.
    var query = [
        'chs=300x140',
        'cht=p3',
        'chts=' + ['000000', 16].join(','),
        'chco=' + colors.join('|'),
        'chd=t:' + data.join(','),
        'chdl=' + labels.join('|'),
        'chdls=000000,14',
        'chp=1.6',
        'chds=0,' + largestSingleCategory,
    ].join('&');
    var image = document.createElement('img');
    image.src = 'http://chart.apis.google.com/chart?' + query;
    return image;

}