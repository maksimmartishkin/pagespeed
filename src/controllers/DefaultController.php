<?php
/**
 * Copyright (c) 2019.
 *
 * author: maksim martishkin
 */

namespace maksimmartishkin\pagespeed\controllers;

use maksimmartishkin\pagespeed\Pagespeed;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Default controller for the `pagespeedInsightUrl` module
 */
class DefaultController extends Controller
{
	/**
	 * @var \maksimmartishkin\pagespeed\Pagespeed
	 */
	public $module;

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

	public function actionRequestpsi() {
		$this->enableCsrfValidation = false;
		$mobile = $desktop = $scope_mob = $scope_desk = $screenshot_mob = $screenshot_desk = $mobile_json = $desktop_json = $mobile_stats = $desktop_stats = false;
		$apikey_google = $this->module->apiKey;
		$locale = 'ru';
		$screenshot = 'true';
		$snapshots = 'true';

		if(Yii::$app->request->post() || Yii::$app->request->get()) {
			$url_post = Yii::$app->request->post('check_url');
			$url_get = Yii::$app->request->get('url');
			$postdataarmob = [
				'url' => $url_post ? $url_post : $url_get,
				'locale' => $locale,
				'strategy' => 'mobile',
				'screenshot' => $screenshot,
				'snapshots' => $snapshots,
				'key' => $apikey_google,
			];
			$exec_mob = $this->module->curl($postdataarmob);
			$exec_mob_ar = json_decode($exec_mob, true);

			$postdataardesc = [
				'url' => $url_post ? $url_post : $url_get,
				'locale' => $locale,
				'strategy' => 'desktop',
				'screenshot' => $screenshot,
				'snapshots' => $snapshots,
				'key' => $apikey_google,
			];
			$exec_desc = $this->module->curl($postdataardesc);
			$exec_desc_ar = json_decode($exec_desc, true);
//			$exec = [
//				'mobile' => $exec_mob_ar,
//				'desctop' => $exec_desc_ar,
//			];

			$mobile_json = json_encode($exec_mob_ar['pageStats']);
			$desktop_json = json_encode($exec_desc_ar['pageStats']);

			$exec_psi_mob = $exec_mob_ar['formattedResults']['ruleResults'];
			$exec_psi_desc = $exec_desc_ar['formattedResults']['ruleResults'];
			$scope_mob = $exec_mob_ar['ruleGroups']['SPEED']['score'];
			$scope_desk = $exec_desc_ar['ruleGroups']['SPEED']['score'];
			$screenshot_mob = $exec_mob_ar['screenshot'];
			$screenshot_desk = $exec_desc_ar['screenshot'];
			$mobile_stats = $exec_mob_ar['pageStats'];
			$desktop_stats = $exec_desc_ar['pageStats'];
			$mobile = $this->module->PaseSpeedInsightArray($exec_psi_mob);
			$desktop = $this->module->PaseSpeedInsightArray($exec_psi_desc);

		} else {
			throw new HttpException(404 ,'Данная страница недоступна');
		}

		return $this->render('request-psi', [
			'mobile' => $mobile,
			'desktop' => $desktop,
			'scope_mob' => $scope_mob,
			'scope_desk' => $scope_desk,
			'screenshot_mob' => $screenshot_mob,
			'screenshot_desk' => $screenshot_desk,
			'mobile_json' => $mobile_json,
			'desktop_json' => $desktop_json,
			'mobile_stats' => $mobile_stats,
			'desktop_stats' => $desktop_stats,
		]);

	}



}
