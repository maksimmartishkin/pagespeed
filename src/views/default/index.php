<?php
/**
 * Copyright (c) 2019.
 *
 * author: maksim martishkin
 */
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\modules\pagespeed\AppAsset;


$title = '<span><i class="fa fa-tachometer"></i></span> Проверка скорости загрузки страницы';
$this->title = $title;
$bundle = new AppAsset();

?>
	<h1 class="text-center">Pagespeed Insight Url <br /><small><?=$this->title?></small></h1>

			<?= Html::beginForm('', 'post', ['id' => 'check_url_form', 'class' => 'form-inline']);?>
			<!--form id="check_url_form" action="<?//=Url::to()?>" method="post"-->
			<div class="row">
				<div class="col-lg-12">
					<div class="input_check_url input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-globe"></span></span>
						<?=Html::textInput('', 'https://martishkin.ru', ['class' => 'form-control input-lg js-check_url', 'placeholder' => 'Введите урл сайта для проверки', 'required' => 'required', 'onkeyup' => 'PageSpeed.isValidURL(this.value)']);?>
						<div class=" ">
							<span class="glyphicon glyphicon-ok form-control-feedback"></span>
						</div>
<!--						<input type="text" class="form-control input-lg">-->
						<span class="input-group-btn">
							<button class="btn btn-primary btn-lg" type="button" onclick="PageSpeed.check()">Проверить!</button>
						</span>
					</div>
				</div>
			</div>

<!--				<div class="form-group">-->
<!--					<div class="col-lg-12 input-group input_check_url">-->
<!--						<span class="input-group-addon"><span class="glyphicon glyphicon-globe"></span></span>-->
<!--						<input type="url" id="check_url" class="form-control input-lg" name="check_url" value="http://" placeholder="Введите урл сайта для проверки" required="required" onkeyup="PageSpeed.isValidURL(this.value)">-->
<!--						<span class="input-group-btn">-->
<!--				</span>-->
<!--					</div>-->
<!--				</div>-->
<!--			<button type="submit" id="check_url_btn" class="btn btn-primary btn-lg" style="height: 36px" onclick="PageSpeed.check(this)">Проверить</button>-->

			<!--/form-->
			<?= Html::endForm();?>

			<?/*<div class="spinner_check_url text-center">
		<img src="/images/30.gif" alt="Загрузка" />
	</div>*/?>
			<div class="container js_result_check_url"></div>


<?php
//$exec_psi = [];
//echo '<pre>';print_r($exec_psi['mobile']);echo '</pre>';

//$url_request_psi = Url::to(['requestpsi']);
//var_dump($url_request_psi);
//$url_request_geocode_long_lag = Url::home(true) . Url::to('api/request-geocode-address');
//$url_request_taxi_routeinfo = Url::home(true) . Url::to('api/request-taxi-routeinfo');

/*$this->registerJs(<<<JS
	$('#check_url_btn').on('click', function(event) {
		event.preventDefault();
		$('.spinner_check_url').show('fade');
		$('#check_url_btn').attr('disabled', 'disabled');
		$('.js_result_check_url').empty();
		var check_url = $('#check_url').val();
		console.log(check_url);
		$.ajax({
			type: 'POST',
			url: '$url_request_psi',
			data: 'check_url=' + check_url,
			//dataType: 'json', 
			success: function(result){
console.log(result);
				$('.js_result_check_url').html($(result).find('.site-about').html());
				var result_psi_mob = $(result).find('.js_scope_json_mob').html();
				var result_psi_desk = $(result).find('.js_scope_json_desk').html();
				var img_result_psi_mob = StatsImgResult(JSON.parse(result_psi_mob));
				var img_result_psi_desk = StatsImgResult(JSON.parse(result_psi_desk));
				$('#img_result_psi_mob').html(img_result_psi_mob);
				$('#img_result_psi_desk').html(img_result_psi_desk);
				$('#img_result_psi_mob').children().addClass('img_psi_mob');
				$('#img_result_psi_desk').children().addClass('img_psi_desk');
				$('.spinner_check_url').hide('fade');
				$('#check_url_btn').attr('disabled', false);
			}
		});
	});




JS
);*/

?>