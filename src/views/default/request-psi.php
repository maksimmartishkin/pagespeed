<?php
/**
 * Copyright (c) 2019.
 *
 * author: maksim martishkin
 */


/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

//$title = 'Проверка скорости загрузки страницы';
//$this->title = $title;
//$this->params['breadcrumbs'][] = ['label'=>'Сервисы', 'url'=>['/services/']];
//$this->params['breadcrumbs'][] = $title;
//$locale = setlocale(LC_ALL, "ru_RU.");

//
if($scope_mob < 60) {
	$scope_mob_color = 'scope_desk_color_danger';
} else if($scope_mob > 59 || $scope_mob < 80) {
	$scope_mob_color = 'scope_desk_color_warming';
} else if($scope_mob > 79) {
	$scope_mob_color = 'scope_desk_color_success';
}
if($scope_desk < 60) {
	$scope_desk_color = 'scope_desk_color_danger';
} else if($scope_desk > 59 || $scope_desk < 80) {
	$scope_desk_color = 'scope_desk_color_warming';
} else if($scope_desk > 79) {
	$scope_desk_color = 'scope_desk_color_success';
}

?>
<div class="js_scope_json_mob"><?=$mobile_json?></div>
<div class="js_scope_json_desk"><?=$desktop_json?></div>
<div class="site-about">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#mobile" data-toggle="tab"><i class="fa fa-mobile fa-lg" aria-hidden="true"></i> Для мобильных <span class="badge <?=$scope_mob_color?> text-center"><?=$scope_mob?>/100</span></a></li>
		<li><a href="#desktop" data-toggle="tab"><i class="fa fa-desktop fa-lg" aria-hidden="true"></i> Для ПК <span class="badge <?=$scope_desk_color?> text-center"><?=$scope_desk?>/100</span></a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane active" id="mobile">
			<div class="row">
				<div class="col-xs-12">
					<div id="img_result_psi_mob" class="col-sm-3 col-xs-12"></div>
					<div class="js_text_stast_psi_mob col-sm-2 col-xs-12">
						<?php if((array_key_exists('javascriptResponseBytes', $mobile_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">JavaScript </span><?=Yii::$app->formatter->asShortSize($mobile_stats['javascriptResponseBytes'], 2)?></p>
						<?php endif; ?>
						<?php if((array_key_exists('imageResponseBytes', $mobile_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">Images </span><?=Yii::$app->formatter->asShortSize($mobile_stats['imageResponseBytes'], 2)?></p>
						<?php endif; ?>
						<?php if((array_key_exists('cssResponseBytes', $mobile_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">CSS </span><?=Yii::$app->formatter->asShortSize($mobile_stats['cssResponseBytes'], 2)?></p>
						<?php endif; ?>
						<?php if((array_key_exists('htmlResponseBytes', $mobile_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">HTML </span><?=Yii::$app->formatter->asShortSize($mobile_stats['htmlResponseBytes'], 2)?></p>
						<?php endif; ?>
						<?php if((array_key_exists('textResponseBytes', $mobile_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">Text </span><?=Yii::$app->formatter->asShortSize($mobile_stats['textResponseBytes'], 2)?></p>
						<?php endif; ?>
						<?php if((array_key_exists('otherResponseBytes', $mobile_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">Other </span><?=Yii::$app->formatter->asShortSize($mobile_stats['otherResponseBytes'], 2)?></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-12">
				<div class="panel-group" id="accordion">
					<?php if($mobile) : ?>
					<?php foreach($mobile as $key => $mob) : ?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#<?=preg_replace('/[^a-zA-Z\s]/', '', $key)?>_mob">
										<?=!empty($mob['localizedRuleName']) ? $mob['localizedRuleName'] : ''?>
									</a>
								</h4>
							</div>
							<div id="<?=preg_replace('/[^a-zA-Z\s]/', '', $key)?>_mob" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="panel panel-default">
										<div class="panel-heading"><?=$mob['summary'];?></div>
										<div class="panel-body">
											<?php //echo '<pre>';print_r(sizeOf($mob['header_ar']));echo '</pre>'; ?>
											<?php if(!empty($mob['header_ar'])) : ?>
											<?php for($i = 0; $i < sizeOf($mob['header_ar']); $i++) :?>
												<p class="small"><?=!empty($mob['header_ar']) ? $mob['header_ar'][$i] : ''?></p>
													<?php //var_dump($mob['header_ar'][$i]);die();?>
												<?php if(!empty($mob['urls']) && array_key_exists($i, $mob['urls'])) : ?>
													<?php if(!empty($mob['urls'][$i])) : ?>
													<ul>
														<?php if(is_array($mob['urls'][$i])) : ?>
														<?php for($j = 0; $j < sizeOf($mob['urls'][$i]); $j++) :?>
															<li class="small"><?=$mob['urls'][$i][$j]?></li>
															<?php //echo '<pre>';print_r($result_text);echo '</pre>';?>
														<?php endfor; ?>
														<?php else : ?>
															<li class="small"><?=$mob['urls'][$i]?></li>
														<?php endif; ?>
													</ul>
													<?php endif; ?>
												<?php endif; ?>
											<?php endfor; ?>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-4 col-sm-12">
				<?php
				$data_image = str_replace(['_', '-'], ['/', '+'], $screenshot_mob['data']);
				?>
				<img src="data:<?=$screenshot_mob['mime_type']?>;base64,<?=$data_image?>" title="<?//=$exec_psi['mobile']['id']?>" width="100%" />
			</div>
		</div>



		<?php /*Показ данных по проверке desctop`ной версии*/?>
		<div class="tab-pane" id="desktop">
			<div class="row">
				<div class="col-xs-12">
					<div id="img_result_psi_desk" class="col-sm-3 col-xs-12"></div>
					<div class="js_text_stast_psi_desk col-sm-2 col-xs-12">
						<?php if((array_key_exists('javascriptResponseBytes', $desktop_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">JavaScript </span><?=Yii::$app->formatter->asShortSize($desktop_stats['javascriptResponseBytes'], 2)?></p>
						<?php endif; ?>
						<?php if((array_key_exists('imageResponseBytes', $desktop_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">Images </span><?=Yii::$app->formatter->asShortSize($desktop_stats['imageResponseBytes'], 2)?></p>
						<?php endif; ?>
						<?php if((array_key_exists('cssResponseBytes', $desktop_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">CSS </span><?=Yii::$app->formatter->asShortSize($desktop_stats['cssResponseBytes'], 2)?></p>
						<?php endif; ?>
						<?php if((array_key_exists('htmlResponseBytes', $desktop_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">HTML </span><?=Yii::$app->formatter->asShortSize($desktop_stats['htmlResponseBytes'], 2)?></p>
						<?php endif; ?>
						<?php if((array_key_exists('textResponseBytes', $desktop_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">Text </span><?=(array_key_exists('textResponseBytes', $desktop_stats)) ? Yii::$app->formatter->asShortSize($desktop_stats['textResponseBytes'], 2) : '?'?></p>
						<?php endif; ?>
						<?php if((array_key_exists('otherResponseBytes', $desktop_stats))) : ?>
							<p class="text_stats_psi"><span class="hidden-sm hidden-md hidden-lg">Other </span><?=Yii::$app->formatter->asShortSize($desktop_stats['otherResponseBytes'], 2)?></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-12">
				<div class="panel-group" id="accordion">
					<?php foreach($desktop as $key => $desk) : ?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#<?=preg_replace('/[^a-zA-Z\s]/', '', $key)?>_desk">
										<?=$desk['localizedRuleName']?>
									</a>
								</h4>
							</div>
							<div id="<?=preg_replace('/[^a-zA-Z\s]/', '', $key)?>_desk" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="panel panel-default">
										<div class="panel-heading"><?=$desk['summary'];?></div>
										<div class="panel-body">
											<?php //echo '<pre>';print_r(sizeOf($mob['header_ar']));echo '</pre>'; ?>
											<?php for($i = 0; $i < sizeOf($desk['header_ar']); $i++) :?>
												<p class="small"><?=$desk['header_ar'][$i]?></p>
												<?php if(array_key_exists($i, $desk['urls'])) : ?>
													<ul>
														<?php if(is_array($desk['urls'][$i])) : ?>
														<?php for($j = 0; $j < sizeOf($desk['urls'][$i]); $j++) :?>
															<li class="small"><?=$desk['urls'][$i][$j]?></li>
															<?php //echo '<pre>';print_r($result_text);echo '</pre>';?>
														<?php endfor; ?>
														<?php else : ?>
															<li class="small"><?=$desk['urls'][$i]?></li>
														<?php endif; ?>
													</ul>
												<?php endif; ?>
											<?php endfor; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="col-md-4 col-sm-12">
				<?php
				$data_image = str_replace(['_', '-'], ['/', '+'], $screenshot_desk['data']);
				?>
				<img src="data:<?=$screenshot_desk['mime_type']?>;base64,<?=$data_image?>" title="<?//=$exec_psi['mobile']['id']?>" />
			</div>
		</div>
	</div>
</div>
<?php //endif; ?>

<style>
	.badge {
		display: inline-block;
	}
	.scope_desk_color_danger {
		background: #ea0c0c;
	}
	.scope_desk_color_warming {
		background: #ffd900;
	}
	.scope_desk_color_success {
		background: #00480e;
	}
</style>
<?php
$this->registerJs(<<<JS
	var RESOURCE_TYPE_INFO = [
		{label: 'JavaScript', field: 'javascriptResponseBytes', color: 'e2192c'},
		{label: 'Images', field: 'imageResponseBytes', color: 'f3ed4a'},
		{label: 'CSS', field: 'cssResponseBytes', color: 'ff7008'},
		{label: 'HTML', field: 'htmlResponseBytes', color: '43c121'},
		{label: 'Flash', field: 'flashResponseBytes', color: 'f8ce44'},
		{label: 'Text', field: 'textResponseBytes', color: 'ad6bc5'},
		{label: 'Other', field: 'otherResponseBytes', color: '1051e8'},
	];
	var result_psi_mob = $mobile_json;
	var result_psi_desk = $desktop_json;
	var img_result_psi_mob = StatsImgResult(result_psi_mob);
	var img_result_psi_desk = StatsImgResult(result_psi_desk);
	console.log(img_result_psi_mob);
	console.log(img_result_psi_desk);

	function StatsImgResult(stats) {
		console.log('OK');
		var labels = [];
		var data = [];
		var colors = [];
		var totalBytes = 0;
		var largestSingleCategory = 0;
		for (var i = 0, len = RESOURCE_TYPE_INFO.length; i < len; ++i) {
	    	var label = RESOURCE_TYPE_INFO[i].label;
			console.log(label);
			var field = RESOURCE_TYPE_INFO[i].field;
			var color = RESOURCE_TYPE_INFO[i].color;
			if (field in stats) {
				var val = Number(stats[field]);
				totalBytes += val;
				if (val > largestSingleCategory) largestSingleCategory = val;
					labels.push(label);
					data.push(val);
					colors.push(color);
			}
		}
		// Construct the query to send to the Google Chart Tools.
		var query = [
			'chs=300x140',
			'cht=p3',
			'chts=' + ['000000', 16].join(','),
			'chco=' + colors.join('|'),
			'chd=t:' + data.join(','),
			'chdl=' + labels.join('|'),
			'chdls=000000,14',
			'chp=1.6',
			'chds=0,' + largestSingleCategory,
		].join('&');
		var i = document.createElement('img');
		i.src = 'http://chart.apis.google.com/chart?' + query;
		console.log(i);
		//$('#img_result_psi').html(i);
		var img_result_psi = document.getElementById('img_result_psi');
		img_result_psi.innerHtml = i;/**/
		return i;

	}
JS
);
//echo '<pre>';print_r($mobile);echo '</pre>';
//echo '<pre>';print_r($desktop);echo '</pre>';

?>
</div>
