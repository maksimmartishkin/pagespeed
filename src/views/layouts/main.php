<?php
/**
 * Copyright (c) 2019.
 *
 * author: maksim martishkin
 */

/* @var $this \yii\web\View */
/* @var $content string */

//use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use maksimmartishkin\pagespeed\PagespeedAsset;

$bundle = PagespeedAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keyword" content="<?=Yii::$app->params['keyword'];?><?/*Мартышкин Максим, Мартышкин Максим Сергеевич, Мартышкин, Мартышкин Максим web-разработка, Мартышкин Максим создание сайтов, программист, web-разработка*/?>" />
    <meta name="description" content="<?=Yii::$app->params['description'];?><?/*Мартышкин Максим Сергеевич - Web-разработчик. Создание сайтов на фрейморке Yii 1 и CMS 1С-Bitrix. Создание компонентов и модулей на платформе 1C-Bitrix*/?>" />
    <meta name="author" content="Мартышкин Максим" />
    <?= Html::csrfMetaTags() ?>
    <title><?= /*($meta['title']) ? Html::encode($meta['title']) :*/ Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php /*<!--meta property="og:image" content="https://optipic.io/optipic-logo-200-200.png" />

	<meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
	<meta http-equiv="pragma" content="no-cache" /-->*/?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <div class="container">
		<?= $content ?>
		<div class="spinner_check_url text-center" style="display: none;">
			<div class="jumbotron">
				<div class="sub_spinner_check_url">
					<div class="waiting_text">Подождите, идет проверка скорости загрузки сайта</div>
					<img src="<?=$bundle->baseUrl; ?>/30.gif" alt="Загрузка" />
					<h3 class="waiting_text">Скоро появятся результаты</h3>
					<span class="sr-only">Загрузка...</span>
				</div>
			</div>
		</div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
